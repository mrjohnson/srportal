Purpose
=================== 
   I'm working on a site related to the Shadowrun RPG. Though, I'm going to
 be using it as a personal site as well. I'll probably be adding a blog,
 along with a lot of other content that I think of as I go along. There
 will definitely be a few Shadowrun characters on there.. I don't know.
 Just the stuff I'm coming up with as I go along. -- Mr. Johnson.

Updates
----------------
   I will post a bit of information about updates down here, but you can
 also find them here on twitter: [@walteRobinson](https://www.twitter.com/walteRobinson)
   I'm a frequent twitter user, so you can often contact me there and get
 a quicker response than if you contacted me via email. I'm a user here on codepen.io as well: [@faded](https://www.codepen.io/faded) -- Mr. Johnson.
