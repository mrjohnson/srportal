$(document).ready(function() {
    $("div#nav-container ul li span").click(function() {
        var is_down = false;
        if($(this).hasClass("down")) {
            var is_down = true;
        }

        $("div#nav-container ul").children("li").children("span.down").each(function(i) {
            $(this).parent().children("ul").slideUp("fast");
            $(this).toggleClass("down");
           })

         if(is_down) {
             $(this).parent().children("ul").slideUp("fast").show();
         }

         else {
             $(this).parent().children("ul").slideDown("fast").show();
         }

         $(this).toggleClass("down");
    });
});
